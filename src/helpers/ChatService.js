import { webSocket, WebSocketSubject } from 'rxjs/webSocket';

const CHAT_WSS_URL = 'wss://x3gxvgzj18.execute-api.us-east-1.amazonaws.com/Prod'

let chatSocket = new WebSocketSubject(null);
let attendeeId = '';

export const init = async (joinResponse) => {
    console.log(joinResponse)
    const meeting = joinResponse.JoinInfo.Meeting;
    const attendee = joinResponse.JoinInfo.Attendee;

    attendeeId = attendee.AttendeeId;
    chatSocket = webSocket(`${CHAT_WSS_URL}?MeetingId=${meeting.MeetingId}&AttendeeId=${attendee.AttendeeId}&JoinToken=${attendee.JoinToken}`);
    console.log(chatSocket);
}

export const sendData = async (data) => {
    const dataLoad =
    {
      data: JSON.stringify({ type: 'chat-message', payload: { attendeeId: attendeeId, ...data} }),
      message: 'sendmessage'
    };
    console.log(dataLoad);
    return chatSocket.next(dataLoad);
}

export const getChatObservable = async (data) => {
    return chatSocket.asObservable();
}