import { DefaultModality } from 'amazon-chime-sdk-js';
import { BehaviorSubject } from 'rxjs';

const meetingDetails = new BehaviorSubject(null);
const joinResponse = new BehaviorSubject(null);
const meetingSessionStore = new BehaviorSubject(null);
const isMeetingRecorder = new BehaviorSubject(null);
const addedParticipant = new BehaviorSubject(null);
const removedParticipant = new BehaviorSubject(null);
const meetingManager = new BehaviorSubject(null);
const participants = new BehaviorSubject(null);
const isCurrentAttendeeHost = new BehaviorSubject(null);

const BASEURL = 'https://f686p7htc3.execute-api.us-east-1.amazonaws.com/Prod/';

export const setMeetingDetails = async (title, attendeeName, region, password, meetingId, attendeeType, attendeeId) => {
    let value = {
      title: title,
      attendeeName: attendeeName,
      region: region,
      password: password,
      meetingId: meetingId,
      attendeeType: attendeeType,
      attendeeId: attendeeId
    }
    meetingDetails.next(value);
  }

export const addParticipant = async({
  volume,
  muted,
  signalStrength,
  attendeeId,
}) => {
  console.log('adding participant');
  const _participants = participants.getValue() || {};
  
  if(_participants && _participants.hasOwnProperty(attendeeId)) {
    _participants[attendeeId].volume = volume; // a fraction between 0 and 1
    _participants[attendeeId].muted = muted; // A booolean
    _participants[attendeeId].signalStrength = signalStrength;
  } else {
    let attendeeName = '';
    let attendeeType = '';
    const meetingSession = await getMeetingSession();
    console.log('session:', meetingSession);
    if(meetingSession && meetingSession.configuration) {
      const meetingId = meetingSession.configuration.meetingId;
      console.log(attendeeId);
      const baseAttendeeId = new DefaultModality(attendeeId).base();
      if(!_participants.hasOwnProperty(baseAttendeeId)) {
        const attendeeDetailsResponse = await promiseParticipantDetails(meetingId, baseAttendeeId);
        console.log('attendeeDetailsResponse: ', attendeeDetailsResponse);
        attendeeName = attendeeDetailsResponse.AttendeeInfo?.Name || '';
        attendeeType =
            attendeeDetailsResponse.AttendeeInfo?.AttendeeType || '';
        console.log('Setting attendeeName to be: ', attendeeName);
        console.log('Setting attendeeType to be: ', attendeeType);
        if (attendeeName === 'Meeting Recorder') {
          attendeeType = 'Recorder';
        }

        const att = {
          attendeeId,
          volume,
          muted,
          signalStrength,
          attendeeName,
          attendeeType,
        };
        console.log('ADDING NEW PARTICIPANT TO THE STORE!: ', att);
        _participants[attendeeId] = att;

        const currentAttendeeId = meetingSession.configuration.credentials.attendeeId;
        console.log('currentAttendeeId: ', currentAttendeeId);
        console.log('attendeeId: ', attendeeId);
        if (currentAttendeeId === attendeeId) {
          console.log('Found currentAttendeeId === attendeeId');
          if (attendeeType === 'Host') {
            console.log('currentAttendeeId is HOST!');
            updateIsCurrentAttendeeHost(true);
          } else {
            console.log('currentAttendeeId is NOT HOST!');
            updateIsCurrentAttendeeHost(false);
          }
        }
        addedParticipant.next(att);
        participants.next(_participants);
      } else {
        const contentParticipantName =
        _participants[baseAttendeeId].attendeeName + ' [CONTENT]';
        let att = {
          attendeeId,
          volume,
          muted,
          signalStrength,
          attendeeName: contentParticipantName,
          attendeeType: 'Content',
        };
        _participants[attendeeId] = att;
        addedParticipant.next(att);
        participants.next(_participants);
      }
    }
  }
}

export const promiseParticipantDetails = async (meetingId, attendeeId) => {
  let url = `${BASEURL}attendee?meetingId=${encodeURIComponent(
    meetingId
  )}&attendee=${encodeURIComponent(
    attendeeId
  )}`;
  let response = await fetch(url);
  let data = await response.json();
  return data;
}

export const updateIsCurrentAttendeeHost = (value) => {
  isCurrentAttendeeHost.next(value);
}

export const getIsCurrentAttendeeHostObservable = () => {
  return isCurrentAttendeeHost.asObservable();
}

export const getMeetingDetails = async () => {
    return meetingDetails.asObservable();
  }

export const getMeetingDetailsValue = async () => {
    return meetingDetails.getValue();
}

export const setJoinResponse = async (response) => {
    joinResponse.next(response);
  }
  
export const getJoinResponse = async () => {
    return joinResponse.getValue();
  }

export const updateMeetingSession = async (value) => {
  meetingSessionStore.next(value);
}

export const getMeetingSession = async () => {
  return meetingSessionStore.getValue();
}

export const getMeetingSessionObservable = async () => {
  return meetingSessionStore.asObservable();
}

export const setMeetingRecorder = async (value) => {
  isMeetingRecorder.next(value);
}

export const getMeetingRecorder = async () => {
  return isMeetingRecorder.asObservable();
}

export const setAddedParticipant = async (value) => {
  return addedParticipant.next(value);
}

export const getAddedParticipantObservable = async () => {
  return addedParticipant.asObservable();
}

export const getRemovedParticipantIdObservable = async () => {
  return removedParticipant.asObservable();
}

export const setMeetingManager = async (value) => {
  return meetingManager.next(value);
}

export const getMeetingManager = async () => {
  return meetingManager.getValue();
}

export const destroySession = async () => {
  meetingSessionStore.complete();
  joinResponse.complete();
  meetingDetails.complete();
}