import React from 'react';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';

export default function Header() {
    return (
        <nav className="navbar navbar-light bg-light">
            <span className="navbar-brand mb-0 h1">Chime Meeting</span>
        </nav>
    )
}