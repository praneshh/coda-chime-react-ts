import React,{ Component } from "react";
import { getAddedParticipantObservable, getRemovedParticipantIdObservable } from "../helpers/MeetingService";

interface Props {};
interface State {
    participants: Array<any>
};

export class Participants extends Component<Props, State> {

    constructor(props) {
        super(props);
        this.state = {
            participants: []
        }
    }

    componentDidMount() {
        this.subscribeToParticipants();
    }

    async subscribeToParticipants() {
        (await getAddedParticipantObservable()).subscribe((newParticipant) => {
            console.log(newParticipant);
            if(newParticipant) {
                const newParticipantAttendeeId = newParticipant.attendeeId;
                const index = this.state.participants.findIndex(
                    (element) => element.attendeeId === newParticipantAttendeeId
                );
                if(index < 0) {
                    let participants = this.state.participants;
                    console.log('pushing', newParticipant, this.state.participants);
                    participants.push(newParticipant);
                    this.setState({
                        participants: participants
                    });
                    console.log(this.state.participants);
                }
            }
        });
        (await getRemovedParticipantIdObservable()).subscribe((removedParticipantId) => {
            console.log(removedParticipantId);
            if(this.state.participants && this.state.participants.length) {
                const indexToRemove = this.state.participants.findIndex(
                    (element) => element.attendeeId === removedParticipantId
                );
                console.log(indexToRemove);
                if( indexToRemove > -1) {
                    let participants = this.state.participants;
                    participants.splice(indexToRemove, 1);
                    this.setState({
                        participants: participants
                    })
                    console.log(
                        'participants after removing', this.state.participants
                    )
                }
            }
        })
    }

    render() {
        return (
            <div className="participants-wrapper container h-100">
            <div className="card h-100 d-flex flex-column">
                <header className="card-header">
                    <h4>Participants</h4>
                </header>
                <div className="card-body">
                    {
                        this.state.participants.map((participant, i) => 
                            <div key={i}>
                                <p>{participant.attendeeName} - {participant.attendeeType}</p>
                            </div>
                        )
                    }
                </div>
            </div>
            </div>
        )
    }
}