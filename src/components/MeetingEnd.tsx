import React from 'react';
import { PrimaryButton } from "amazon-chime-sdk-component-library-react";
import history from '../history';


export default function MeetingEnd() {
    const joinMeeting = async () => {
        history.push('/join');
    }
    return (
        <>
            <h1>Thank you for joining !</h1>
            <h3>Click here to join another meeting</h3>
            <PrimaryButton label="Join a Meeting" onClick={joinMeeting} />
        </>
    )
}