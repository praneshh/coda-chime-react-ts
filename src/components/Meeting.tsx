import React, { useEffect, useState } from 'react';
import './Meeting.css';
import {
  AudioInputControl,
  AudioOutputControl,
  ContentShareControl,
  VideoInputControl,
  VideoTileGrid,
  LeaveMeeting, useMeetingManager
} from 'amazon-chime-sdk-component-library-react';
import { addParticipant, getMeetingDetails, getMeetingDetailsValue, getMeetingSessionObservable, setJoinResponse, setMeetingDetails, updateMeetingSession} from '../helpers/MeetingService';
import history from '../history';
import { Chat } from './Chat';
import { Participants } from './Participants';

const BASEURL = 'https://f686p7htc3.execute-api.us-east-1.amazonaws.com/Prod/';


export default function Meeting() {

  const [isMeetingLoaded, setIsMeetingLoaded] = useState(false);
  const meetingManager = useMeetingManager();

  const reinitializeMeeting  = async () => {
    let search = window.location.search;
    let params = new URLSearchParams(search);
    const meetingId = params.get('meetingId');
    const name = params.get('name');
    const region = params.get('region');
    const password = params.get('password');
    const attendeeType = params.get('attendeeType');
    let url = `${BASEURL}join?title=${meetingId}&name=${name}&region=${region}&password=${password}&attendeeType=${attendeeType}&record=true`;

    const requestOptions = {
      method: 'POST',
      body: JSON.stringify({})
    };
    const response = await fetch(url, requestOptions);
    console.log(response);
    const data = await response.json();
    console.log(data);

    const joinData = {
      meetingInfo: data.JoinInfo.Meeting,
      attendeeInfo: data.JoinInfo.Attendee
    };

    await setJoinResponse(data);

    await setMeetingDetails(
      data.JoinInfo.Meeting.Meeting.ExternalMeetingId,
      name, 
      region,
      password,
      meetingId,
      attendeeType,
      data.JoinInfo.Attendee.Attendee.AttendeeId
    );
    await meetingManager.join(joinData);
    await meetingManager.start();
    await updateMeetingSession(meetingManager.meetingSession);
    await initializeParticipants();
    setIsMeetingLoaded(true);
  }

  const initializeParticipants = async() => {
    const presentAttendeeId = (await getMeetingDetailsValue()).attendeeId;
    console.log(presentAttendeeId);
    await addParticipant({
      volume: 0,
      muted: false,
      signalStrength: 0,
      attendeeId: presentAttendeeId,
    })
  }

  const leaveMeeting = async () => {
    (await getMeetingDetails()).subscribe(async (res) => {
      let url =`${BASEURL}end?title=${res.title}&password${res.password}`;
      let requestOptions = {
        method: 'POST',
        body: JSON.stringify({})
      }
      await fetch(url, requestOptions);
    history.push('/end')
    })
  }

  const init = async () => {
    (await getMeetingSessionObservable()).subscribe(async (res) => {
      if(res) {
        console.log('meeting session present', res);
        initializeParticipants();
        await meetingManager.start();
      } else {
        console.log('reinitializing');
        await reinitializeMeeting();
      }
    });
  }

  useEffect(() => {
    init();
  })

  return (
    <>
    <div className="row">
        <div className="col-8">
            <div className="local-video">
              <div className="meeting-controls">
                <AudioInputControl />
                <AudioOutputControl />
                <ContentShareControl />
                <VideoInputControl />
                <LeaveMeeting width="2rem" onClick={leaveMeeting} />
              </div>
              <VideoTileGrid />
            </div>
        </div>
        <div className="col-4">
            <div>
              <div className="chat-wrapper">
                <Chat />
              </div>
              <div className="participants-wrapper">
                <Participants />
              </div>
            </div>
        </div>
      </div>
    </>
  )
};
