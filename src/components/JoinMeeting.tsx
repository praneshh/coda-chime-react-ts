import React from 'react';
import {
  useMeetingManager,
  PrimaryButton
} from 'amazon-chime-sdk-component-library-react';
import history from '../history';
import { setMeetingDetails, setJoinResponse, updateMeetingSession, getMeetingDetails, getMeetingDetailsValue } from '../helpers/MeetingService';
import faker from 'faker';


export const JoinMeeting = () => {

  //const [redirectToMeetingPage, setRedirect] = useState(false);

  const meetingManager = useMeetingManager();

  let search = window.location.search;
  let params = new URLSearchParams(search);
  let meetingId = params.get('id');

  const joinMeeting = async () => {
    let id;
    // Fetch the meeting and attendee data from your server
    if(meetingId) {
      id = meetingId;
    } else {
      id = faker.random.alphaNumeric(18)
    }
    let name = faker.name.firstName();
    let region = 'us-east-1';
    let password = 'z4h1784pje';
    let baseurl = 'https://f686p7htc3.execute-api.us-east-1.amazonaws.com/Prod/';
    let attendeeType = meetingId ? 'Participant' : 'Host';
    let url = `${baseurl}join?title=${id}&name=${name}&region=${region}&password=${password}&attendeeType=${attendeeType}&record=true`;
    
    console.log(url);
    const requestOptions = {
      method: 'POST',
      body: JSON.stringify({})
  };
    const response = await fetch(url, requestOptions);
    const data = await response.json();
    console.log(data);

    const joinData = {
      meetingInfo: data.JoinInfo.Meeting,
      attendeeInfo: data.JoinInfo.Attendee
    };

    await setJoinResponse(data);

    await setMeetingDetails(
      data.JoinInfo.Meeting.Meeting.ExternalMeetingId,
      name, 
      region,
      password,
      id,
      attendeeType,
      data.JoinInfo.Attendee.Attendee.AttendeeId
    );

    console.log(await getMeetingDetailsValue());

   

    // Use the join API to create a meeting session
    await meetingManager.join(joinData);
    console.log(meetingManager);

    // At this point you can let users setup their devices, or start the session immediately
    await meetingManager.start();
    console.log(meetingManager);
    updateMeetingSession(meetingManager.meetingSession);
    (await getMeetingDetails()).subscribe(async (res) => {
       history.push({
        pathname: '/meeting',
        search: `?meetingId=${res.meetingId}&name=${res.attendeeName}&password=${res.password}&attendeeType=${res.attendeeType}&region=${res.region}`
      })
    });

  };

  if(meetingId) {
    console.log('joining as participant');
    joinMeeting();
  }

  return (
    <>
    <PrimaryButton label="Join Meeting" onClick={joinMeeting}/>
    </>
  );
}

