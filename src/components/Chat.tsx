import React from "react";
import { getJoinResponse, getMeetingDetailsValue, getMeetingSession, getMeetingSessionObservable } from '../helpers/MeetingService';
import { getChatObservable, init, sendData } from '../helpers/ChatService';
import './Chat.css';

interface Props {};
interface State {
    message: string,
    chatMessages: Array<any>
};

export class Chat extends React.Component<Props, State> {

    name: string;
    currentAttendeeId: string;
    
    constructor(props) {
        super(props);
        this.state = {
            message : '',
            chatMessages: []
        }
        this.onMessageChange = this.onMessageChange.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
        this.onKeyUp = this.onKeyUp.bind(this);
        this.subscribeToMessages = this.subscribeToMessages.bind(this);
    }

    componentDidMount() {
        this.initializeChat();
    }

    async initializeChat() {
        console.log('initializing chat');
        (await getMeetingSessionObservable()).subscribe(async (session: any) => {
            if(session) {
              init((await getJoinResponse()));
              this.name = (await getMeetingDetailsValue()).attendeeName;
              this.currentAttendeeId = (await getMeetingSession()).configuration.credentials.attendeeId;
              this.subscribeToMessages();
            }
          })
    }

    async subscribeToMessages() {
        console.log('subscribing to messages');
        (await getChatObservable()).subscribe((data) => {
            console.log(data);
            console.log(typeof(data));
            let messages = this.state.chatMessages;
            messages.push(data);
            this.setState({
                chatMessages: messages
            })
            console.log('pusing', data);
            console.log(this.state.chatMessages);
        });
    }

    async sendMessage() {
        console.log('sending message');
        console.log(this.state.message);
        if(this.state.message && this.state.message.trim().length >0) {
            sendData({name: this.name, message: this.state.message, messsageType: 'CHAT_MESSAGE'});
            this.setState({
                message: ''
            })
            console.log(this.state.message)
        };
        console.log(this.state.chatMessages);
    }

    onKeyUp(event) {
        if(event.charCode === 13) {
            this.sendMessage();
        }
    }

    onMessageChange(event) {
        this.setState({
            message: event.target.value
        });
    }

    render() {
        return (
            <>
                <div className="chat-wrapper container h-100">
                    <div className="live-chat card d-flex flex-column">
                        <header className="card-header">
                        <h4>Chat</h4>
                        </header>
                        <div className="chat card-body">
                        <div className="chat-history d-flex flex-column">

                        <div className="w-100 chat-message flex-grow-1 overflow-auto">
                            {
                                this.state.chatMessages.map((message,i) => 
                                    <div className="chat-message-content clearfix" key={i}>
                                        <p className="chat-sender-name" >{message.payload.name}</p>
                                        <p className="chat-bubble">{message.payload.message}</p>
                                    </div>
                                )
                            }
                        </div>


                            <div className="input-group mb-3">
                            <input type="text" className="form-control" placeholder="Send Message" aria-label="Recipient's username"
                                aria-describedby="button-addon2" onChange={this.onMessageChange} onKeyPress={this.onKeyUp} value={this.state.message} />
                            <div className="input-group-append">
                                <button className="btn btn-outline-secondary" type="button" id="button-addon2" onClick={this.sendMessage}>Send</button>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
            </>
        )
    }
}