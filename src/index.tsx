import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { ThemeProvider } from 'styled-components';
import { lightTheme, MeetingProvider } from 'amazon-chime-sdk-component-library-react';
import { Router } from 'react-router-dom';
import history from './history';

ReactDOM.render(
  <React.StrictMode>
    <Router history={history}>
    <ThemeProvider theme={lightTheme}>
      <MeetingProvider>
        <App />
      </MeetingProvider>
      </ThemeProvider>
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
