export interface MeetingDetails {
    title: string,
    attendeeName: string,
    region: string,
    password: string,
    meetingId: string,
    attendeeType: string,
    attendeeId: string
}