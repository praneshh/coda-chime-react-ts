import React from 'react';
import { Switch, Route } from 'react-router-dom';
import './App.css';
import Header from './components/Header';
import { JoinMeeting } from './components/JoinMeeting';
import MeetingEnd from './components/MeetingEnd';
import Meeting from './components/Meeting';

class App extends React.Component {
  render() {
    return ( 
        <>
        <Header />
        <Switch>
          <Route path="/meeting" component={Meeting} exact />
          <Route path="/end" component={MeetingEnd} exact />
          <Route path="/join" component={JoinMeeting} exact />
        </Switch>
        </>
    );
  }
}

export default App;
